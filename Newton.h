/* Newton.h: Header for Newton's Fractal
 * Author: Joan Bruguera
 * License: WTFPL */
#ifndef NEWTON_H
#define NEWTON_H

#include "Fractal.h"
#include <stddef.h>
#include <complex.h>

struct Fractal *NewtonFractal_Create(
	double minX, double maxX,
	double minY, double maxY,
	complex double (*iterationFunc)(complex double x),
	size_t nRoots,
	complex double *roots
);

struct Fractal *NewtonFractal_X3m1_Create(void);
struct Fractal *NewtonFractal_X3m2Xp2_Create(void);

#endif // NEWTON_H
