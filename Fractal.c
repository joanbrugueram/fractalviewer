/* Fractal.h: Code for Fractal base class
 * Author: Joan Bruguera
 * License: WTFPL */
#include "Fractal.h"
#include <stdio.h>
#include "GL/glfw.h"

void Fractal_Translate(struct Fractal *fractal, double tx, double ty)
{
	fractal->minX += tx;
	fractal->maxX += tx;
	fractal->minY += ty;
	fractal->maxY += ty;
}

void Fractal_Zoom(struct Fractal *fractal, double scale, double x, double y)
{
	// This transformation has the following properties in the X axis:
	// - The new axis has length (maxX-minX)*scale.
	// - The ratio (x-minX)/(maxX-minX) is conserved.
	//   In other words, when using a mouse, multiple zoom operations
	//   without moving the mouse will zoom towards the same point.
	// Ditto for the Y axis. 
	fractal->minX = scale * (fractal->minX - x) + x;
	fractal->maxX = scale * (fractal->maxX - x) + x;
	fractal->minY = scale * (fractal->minY - y) + y;
	fractal->maxY = scale * (fractal->maxY - y) + y;
}

struct FractalRenderArgs
{
	struct Fractal *fractal;
	uint8_t *pixels;
	int width;
	int height;
	double minX, maxX;
	double minY, maxY;
};

static GLFWCALL void fractalRenderThreadCallback(void *arg)
{
	struct FractalRenderArgs *renderArgs = (struct FractalRenderArgs *)arg;
	renderArgs->fractal->renderArea(renderArgs->fractal, renderArgs->pixels,
	                                renderArgs->width, renderArgs->height,
	                                renderArgs->minX, renderArgs->maxX,
	                                renderArgs->minY, renderArgs->maxY);
}

void Fractal_Render(struct Fractal *fractal, uint8_t *pixels, int width, int height)
{
	// Figure out a good number of threads to use
	int nCores = glfwGetNumberOfProcessors();
	int nChunks = (width * height) / 16384;

	if (nCores == 1 || nChunks <= 1 || height < nCores) // Not worth threading
	{
		fractal->renderArea(fractal, pixels, width, height,
		                    fractal->minX, fractal->maxX,
		                    fractal->minY, fractal->maxY);
		return;
	}

	int nThreads = min(nCores, nChunks);
	printf("Rendering using %i threads\n", nThreads);

	// Create the threads
	int scanlinesPerBlock = height / nThreads;
	double scanlineFractalHeight = (fractal->maxY - fractal->minY) / height;

	struct FractalRenderArgs infos[nThreads];
	GLFWthread threads[nThreads];
	for (int i = 0; i < nThreads; i++)
	{
		int firstScanline = scanlinesPerBlock * i;
		int scanlinesRemaining = height - firstScanline;
		int scanlinesToRender = min(scanlinesPerBlock, scanlinesRemaining);
		int lastScanline = firstScanline + scanlinesToRender;

		infos[i].fractal = fractal;
		infos[i].pixels = pixels + firstScanline * width * 4;
		infos[i].width = width;
		infos[i].height = scanlinesToRender;
		infos[i].minX = fractal->minX;
		infos[i].maxX = fractal->maxX;
		infos[i].minY = fractal->maxY - scanlineFractalHeight * lastScanline;
		infos[i].maxY = fractal->maxY - scanlineFractalHeight * firstScanline;

		threads[i] = glfwCreateThread(fractalRenderThreadCallback, &infos[i]);
		if (threads[i] < 0) // Thread creation failed
		{
			// Abort the already launched threads
			for (int j = 0; j < i; j++)
				glfwDestroyThread(threads[i]);

			fprintf(stderr, "WARNING: Thread creation failed, "
			                "rendering will be slow!\n");

			// Fall back to single threaded rendering
			fractal->renderArea(fractal, pixels, width, height,
			                    fractal->minX, fractal->maxX,
			                    fractal->minY, fractal->maxY);
			return;
		}
	}

	// Wait for the rendering to be done
	for (int i = 0; i < nThreads; i++)
		glfwWaitThread(threads[i], GLFW_WAIT);
}
