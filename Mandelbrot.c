/* Mandelbrot.c: Code for Mandelbrot's Set
 * Author: Joan Bruguera
 * License: WTFPL */
#include "Mandelbrot.h"
#include <stdlib.h>
#include <complex.h>

/*******************************************
 * RENDERING CODE FOR MANDELBROT'S FRACTAL *
 *******************************************/
static int Mandelbrot_MakeColor(complex double c)
{
	complex double z = 0;
	for (int i = 0; i < 255; i++)
	{
		z = z*z+c;
		// TODO pick some better colors
		if (cabs(z) >= 2)
			return makeColor32(0xFF-i, 0xFF-i, 0xFF-i, 0xFF);
	}

	return makeColor32(0x00, 0x00, 0x00, 0xFF); // Likely belongs to the set
}

void MandelbrotFractal_Render(const struct Fractal *fractal, uint8_t *pixels,
                              int width, int height,
                              double minX, double maxX, double minY, double maxY)
{
	uint32_t *pixels32 = (uint32_t *)pixels;
	double sx = (maxX - minX) / width;
	double sy = (maxY - minY) / height;

	double cy = maxY;
	for (int y = height-1; y >= 0; y--, cy -= sy)
	{
		double cx = minX;
		for (int x = 0; x < width; x++, cx += sx)
		{
			*pixels32++ = Mandelbrot_MakeColor(cx+cy*I);
		}
	}
}

/********************************************************
 * CONSTRUCTOR/DESTRUCTOR CODE FOR MANDELBROT'S FRACTAL *
 ********************************************************/
void MandelbrotFractal_Destroy(struct Fractal *fractal)
{
	free(fractal);
}

struct Fractal *MandelbrotFractal_Create(void)
{
	struct Fractal *fractal = malloc(sizeof(struct Fractal));
	if (fractal == NULL)
		return NULL;

	fractal->renderArea = MandelbrotFractal_Render;
	fractal->destroy = MandelbrotFractal_Destroy;
	fractal->minX = -2;
	fractal->maxX = 1;
	fractal->minY = -1;
	fractal->maxY = 1;
	return fractal;
}
