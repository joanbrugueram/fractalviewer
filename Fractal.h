/* Fractal.h: Header for Fractal base class + misc. utils
 * Author: Joan Bruguera
 * License: WTFPL */
#ifndef FRACTAL_H
#define FRACTAL_H

#include <stdint.h>

// TODO: Update the API to allow for parallel rendering on multiple threads

/** This structure is used as the "base class" for other fractals */
struct Fractal
{
	/**
	 * Render the fractal to the specified pixel map (single-threaded).
	 *
	 * pixels = an array of size width*height*4, which should be filled with
	 *          the fractal pixel map (format R8 G8 B8 A8 in memory).
	 * width = width of the pixel map
	 * height = height of the pixel map
	 */
	void (*renderArea)(const struct Fractal *fractal, uint8_t *pixels,
	                   int width, int height,
	                   double minX, double maxX, double minY, double maxY);

	/** Destroy this instance of the fractal */
	void (*destroy)(struct Fractal *fractal);

	/** Rendering area */
	double minX, maxX;
	double minY, maxY;
};

/**
 * Apply a translation to the fractal.
 * tx, ty = Translation vector.
 */
void Fractal_Translate(struct Fractal *fractal, double tx, double ty);

/**
 * Zoom into the fractal.
 * scale = scale factor for the sides of the fractal rectangle
 * x, y = point which is going to stay fixed.
 */
void Fractal_Zoom(struct Fractal *fractal, double scale, double x, double y);

/**
 * Render the fractal to the specified pixel map (multi-threaded).
 *
 * pixels = an array of size width*height*4, which should be filled with
 *          the fractal pixel map (format R8 G8 B8 A8 in memory).
 * width = width of the pixel map
 * height = height of the pixel map
 */
void Fractal_Render(struct Fractal *fractal, uint8_t *pixels, int width, int height);

/********************
 * HELPER FUNCTIONS *
 ********************/
#ifndef ARRAY_LENGTH
#define ARRAY_LENGTH(x) (sizeof((x))/sizeof((x)[0]))
#endif

#ifndef min
inline int min(int x, int y)
{
	return (x < y) ? x : y;
}
#endif

#ifndef max
inline int max(int x, int y)
{
	return (x > y) ? x : y;
}
#endif
 
// Checks if the endianness of this CPU is big endian
// Make sure the compiler is optimizing this function!
inline int isBigEndian(void)
{
	union test_endian
	{
		unsigned char x[4];
		unsigned int n;
	} test = { { 0x00, 0x01, 0x02, 0x03 } };

	return test.n == 0x00010203;
}

// Makes a 32-bit color so that in memory it has R8 G8 B8 A8 order
inline uint32_t makeColor32(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	if (isBigEndian())
		return a | (b << 8) | (g << 16) | (r << 24);
	else
		return r | (g << 8) | (b << 16) | (a << 24);
}

#endif // FRACTAL_H
