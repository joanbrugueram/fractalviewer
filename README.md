Fractal Viewer
==============
A toy fractal viewer for the [Mandelbrot Set](http://en.wikipedia.org/wiki/Mandelbrot_Set), some [Julia Sets](http://en.wikipedia.org/wiki/Julia_Set) and [Newton Fractals](http://en.wikipedia.org/wiki/Newton_fractal).

It has few features but the code is easily extensible.

Features
--------
- Can display the following fractals:
- - Mandelbrot Set
- - Julia Set - Polynomial x^2-0.75
- - Julia Set - Polynomial x^2-I
- - Julia Set - Function x^4-e^(2x)
- - Newton's Fractal - Polynomial x^3-1
- - Newton's Fractal - Polynomial x^3-2x+2
- Can explore the fractal by translating & zooming
- The rendering quality can be adjusted
- The rendered fractal can be saved to a TGA image.

Examples
--------
Newton Fractal x^3-1:

[![Newton Fractal x^3-1](https://bitbucket.org/joanbrugueram/fractalviewer/raw/99a6ee845315394574752916d8563db24a3522e5/Screens/Newton_Thumb.png)](https://bitbucket.org/joanbrugueram/fractalviewer/raw/99a6ee845315394574752916d8563db24a3522e5/Screens/Newton_Big.png)

Zoom of the Mandelbrot Set:

[![Zoom of the Mandelbrot Set](https://bitbucket.org/joanbrugueram/fractalviewer/raw/99a6ee845315394574752916d8563db24a3522e5/Screens/Mandelbrot_Thumb.png)](https://bitbucket.org/joanbrugueram/fractalviewer/raw/99a6ee845315394574752916d8563db24a3522e5/Screens/Mandelbrot_Big.png)

Julia Set x^2-0.75:

[![Julia Set x^2-0.75](https://bitbucket.org/joanbrugueram/fractalviewer/raw/99a6ee845315394574752916d8563db24a3522e5/Screens/Julia_Thumb.png)](https://bitbucket.org/joanbrugueram/fractalviewer/raw/99a6ee845315394574752916d8563db24a3522e5/Screens/Julia_Big.png)

Compilation
-----------
The code is written in standard C99, so any compiler should work (GCC is a good choice). It only has two dependencies, [OpenGL](http://www.opengl.org/) and [GLFW](http://www.glfw.org/).

I've included two scripts for a typical Linux-GCC setup and a typical Windows-MinGW setup.

*NOTE: Visual Studio is not a C99 compiler.*

The code is licensed under the [WTFPL](http://www.wtfpl.net/).

Limitations
-----------
- The rendering speed is decent, but it can be optimized, e.g. with specialized routines and multithreading.
- Zooming is limited because of floating point issues.
