/* Newton.c: Code for Newton's Fractal
 * Author: Joan Bruguera
 * License: WTFPL */
#include "Newton.h"
#include <stdlib.h>

/******************************************
 * PRIVATE STRUCTURE FOR NEWTON'S FRACTAL *
 ******************************************/
struct NewtonFractal
{
	/** Base class members */
	struct Fractal base;
	/** Iteration funcion: x-f(x)/f'(x) */
	complex double (*iterationFunc)(complex double x);
	/* Number of roots of the function */
	size_t nRoots;
	/** Roots of the function */
	complex double *roots;
};

/***************************************
 * RENDERING CODE FOR NEWTON'S FRACTAL *
 ***************************************/
#define TOLERANCE 0.001

static uint32_t NewtonFractal_MakeColor(const struct NewtonFractal *nfractal,
                                        uint32_t *colorList, complex double x)
{
	for (int i = 0; i < 32; i++)
	{
		// Find if it's close enough to one of the roots
		for (size_t j = 0; j < nfractal->nRoots; j++)
		{
			double dist = cabs(x - nfractal->roots[j]);
			if (dist < TOLERANCE)
				return colorList[j] | makeColor32(0x00, 0x00, 0x00, 0xFF-i*8);
		}

		// Iterate Newton's method a few times
		x = nfractal->iterationFunc(x);
	}

	return 0x00000000;
}

void NewtonFractal_Render(const struct Fractal *fractal, uint8_t *pixels,
                          int width, int height,
                          double minX, double maxX, double minY, double maxY)
{
	const struct NewtonFractal *nfractal = (const struct NewtonFractal *)fractal;

	// Pick up some colors to render the fractal
	uint32_t colorList[3];
	colorList[0] = makeColor32(0xFF, 0x00, 0x00, 0x00);
	colorList[1] = makeColor32(0x00, 0xFF, 0x00, 0x00);
	colorList[2] = makeColor32(0x00, 0x00, 0xFF, 0x00);
	if (nfractal->nRoots > 3) { /* TODO */ exit(-1); }

	uint32_t *pixels32 = (uint32_t *)pixels;
	double sx = (maxX - minX) / width;
	double sy = (maxY - minY) / height;

	double cy = maxY;
	for (int y = height-1; y >=0; y--, cy -= sy)
	{
		double cx = minX;
		for (int x = 0; x < width; x++, cx += sx)
		{
			*pixels32++ = NewtonFractal_MakeColor(nfractal, colorList, cx+cy*I);
		}
	}
}

/****************************************************
 * CONSTRUCTOR/DESTRUCTOR CODE FOR NEWTON'S FRACTAL *
 ****************************************************/
void NewtonFractal_Destroy(struct Fractal *fractal)
{
	free(fractal);
}

struct Fractal *NewtonFractal_Create(
	double minX, double maxX,
	double minY, double maxY,
	complex double (*iterationFunc)(complex double x),
	size_t nRoots,
	complex double *roots
)
{
	struct NewtonFractal *nfractal = malloc(sizeof(struct NewtonFractal));
	if (nfractal == NULL)
		return NULL;

	nfractal->base.renderArea = NewtonFractal_Render;
	nfractal->base.destroy = NewtonFractal_Destroy;
	nfractal->base.minX = minX;
	nfractal->base.maxX = maxX;
	nfractal->base.minY = minY;
	nfractal->base.maxY = maxY;
	nfractal->iterationFunc = iterationFunc;
	nfractal->nRoots = nRoots;
	nfractal->roots = roots;
	return (struct Fractal *)nfractal;
}

/**********************************
 * NEWTON'S FRACTAL SAMPLE: X^3-1 *
 **********************************/
static complex double X3m1_Iterate(complex double x)
{
	return x - (x*x*x-1) / (3*x*x);
}

static complex double X3m1_Roots[] = {
	+1.00000000000000000000+0.00000000000000000000*I,
	-0.50000000000000000000-0.86602540378443864676*I,
	-0.50000000000000000000+0.86602540378443864676*I,
};

struct Fractal *NewtonFractal_X3m1_Create(void)
{
	return NewtonFractal_Create(
		-40.0, 40.0,
		-40.0, 40.0,
		X3m1_Iterate, ARRAY_LENGTH(X3m1_Roots), X3m1_Roots
	);
}

/*************************************
 * NEWTON'S FRACTAL SAMPLE: X^3-2X+2 *
 *************************************/
static complex double X3m2Xp2_Iterate(complex double x)
{
	return x - (x*x*x-2*x+2) / (3*x*x-2);
}

static complex double X3m2Xp2_Roots[] = {
	-1.76929235423863141524+0.00000000000000000000*I,
	+0.88464617711931570762-0.58974280502220550165*I,
	+0.88464617711931570762+0.58974280502220550165*I,
};

struct Fractal *NewtonFractal_X3m2Xp2_Create(void)
{
	return NewtonFractal_Create(
		-40.0, 40.0,
		-40.0, 40.0,
		X3m2Xp2_Iterate, ARRAY_LENGTH(X3m2Xp2_Roots), X3m2Xp2_Roots
	);
}
