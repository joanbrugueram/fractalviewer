/* Tga.h: Header for saving a RGBA8 buffer to a TGA file
 * Author: Joan Bruguera
 * License: WTFPL */

#ifndef TGA_H
#define TGA_H

#include <stdint.h>

int SaveToTGA(const char *fileName, uint8_t *pix, int width, int height);

#endif // TGA_H
