/* Julia.h: Header for Julia Set
 * Author: Joan Bruguera
 * License: WTFPL */
#ifndef JULIA_H
#define JULIA_H

#include "Fractal.h"
#include <complex.h>

struct Fractal *JuliaFractal_Create(
	double minX, double maxX,
	double minY, double maxY,
	complex double (*iterationFunc)(complex double x),
	double escapeRadius
);

struct Fractal *JuliaFractal_X2m075_Create(void);
struct Fractal *JuliaFractal_X2mI_Create(void);
struct Fractal *JuliaFractal_X4me2X_Create(void);

#endif // JULIA_H
