/* Mandelbrot.h: Header for Mandelbrot's Set
 * Author: Joan Bruguera
 * License: WTFPL */
#ifndef MANDELBROT_H
#define MANDELBROT_H

#include "Fractal.h"

struct Fractal *MandelbrotFractal_Create(void);

#endif // MANDELBROT_H
