/* Tga.c: Code for saving a RGBA8 buffer to a TGA file
 * Author: Joan Bruguera
 * License: WTFPL */
#include "Tga.h"
#include <stdio.h>

int SaveToTGA(const char *fileName, uint8_t *pix, int width, int height)
{
	// Sanity checks
	if (width <= 0 || width >= 0xFFFF)
		return 0;
	if (height <= 0 || height >= 0xFFFF)
		return 0;

	FILE *f = fopen(fileName, "wb");
	if (f == NULL)
		return 0;

	// Write a RGBA8 TGA image
	// See http://paulbourke.net/dataformats/tga/ for details
	fputc(0x00, f);
	fputc(0x00, f);
	fputc(0x02, f);
	fputc(0x00, f); fputc(0x00, f);
	fputc(0x00, f); fputc(0x00, f);
	fputc(0x00, f);
	fputc(0x00, f); fputc(0x00, f);
	fputc(0x00, f); fputc(0x00, f);
	fputc(width, f); fputc(width >> 8, f);
	fputc(height, f); fputc(height >> 8, f);
	fputc(32, f);
	fputc(8, f);

	uint8_t buffer[4096];
	for (int i = 0; i < width*height*4; i += sizeof(buffer))
	{
		int sz = sizeof(buffer);
		if (width*height*4-i < sz)
			sz = width*height*4-i;

		// Convert RGBA8 -> BGRA8
		for (int j = 0; j < sz; j += 4)
		{
			buffer[j+0] = pix[i+j+2];
			buffer[j+1] = pix[i+j+1];
			buffer[j+2] = pix[i+j+0];
			buffer[j+3] = pix[i+j+3];
		}

		fwrite(buffer, 1, sz, f);
	}

	if (ferror(f))
	{
		fclose(f);
		remove(fileName);
		return 0;
	}

	fclose(f);
	return 1;
}
