/* FractalViewer.c: User interface implementation of the Fractal Viewer
 * Author: Joan Bruguera
 * License: WTFPL */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#if defined (__WIN32__) || defined(_WIN32)
	#include <windows.h> // OpenGL on Windows needs windows.h
#endif
#include <GL/gl.h>
#include <GL/glfw.h>
#include "Tga.h"
#include "Fractal.h"
#include "Mandelbrot.h"
#include "Julia.h"
#include "Newton.h"

/** The fractal we're currently displaying */
static struct Fractal *fractal = NULL;
/** Information and pixel data of the current fractal render */
static int renderWidth = 0, renderHeight = 0;
static uint8_t *renderData = NULL;
/** OpenGL identifier of a texture containing the above image. */
// This texture stays binded through the whole program
static GLuint texture;

/** Set to zero to cleanly exit the program as soon as possible. */
static int running = 1;
/** Set to nonzero to redraw as soon as possible. */
static int redraw = 0;

/** Set to nonzero if the right button is clicked (dragging fractal) */
static int dragging = 0;
/** Position of the mouse on the latest mouse position callback (X,Y) */
static int lastMousePos[2];
/** Position of the mouse wheel on the latest mouse wheel callback */
static int lastMouseWheelPos;

/************
 * UTIITIES *
 ************/

/**
 * Does linear interpolation of the point x from the interval
 * [src[0], src[1]] to the interval [dst[0], dst[1]].
 * In other words, evaluates a*x+b, where a and b satisfy
 * - a*src[0]+b == dst[0]
 * - a*src[1]+b == dst[1]
 */
static double linearInterpolation(double x, double src[2], double dst[2])
{
	double range01 = (x - src[0]) / (src[1] - src[0]);
	return range01 * (dst[1] - dst[0]) + dst[0];
}

/**
 * Converts a point from window coordinates to fractal coordinates.
 */
static void windowToFractalCoords(int mouseCoords[2], double fractalCoords[2])
{
	// Note that the mouse coordinates have (0,0) -> top left corner
	// We have to "reverse" the y coordinate when converting to fractal coords
	int windowWidth, windowHeight;
	glfwGetWindowSize(&windowWidth, &windowHeight);

	fractalCoords[0] = linearInterpolation(mouseCoords[0],
		(double[]){0, windowWidth},
		(double[]){fractal->minX, fractal->maxX});
	fractalCoords[1] = linearInterpolation(windowHeight - mouseCoords[1],
		(double[]){0, windowHeight},
		(double[]){fractal->minY, fractal->maxY});
}

/**
 * Converts a vector from window coordinates to fractal coordinates.
 */
static void windowToFractalVector(int mouseVector[2], double fractalVector[2])
{
	// For the same reason as above, y works in reverse
	int windowWidth, windowHeight;
	glfwGetWindowSize(&windowWidth, &windowHeight);

	fractalVector[0] = linearInterpolation(mouseVector[0],
		(double[]){0, windowWidth},
		(double[]){0, fractal->maxX - fractal->minX});
	fractalVector[1] = linearInterpolation(-mouseVector[1],
		(double[]){0, windowHeight},
		(double[]){0, fractal->maxY - fractal->minY});
}

/**********************
 * FRACTAL OPERATIONS *
 **********************/

/**
 * Renders the fractal and updates the associated OpenGL texture.
 */
static void renderFractal(void)
{
	printf("Rendering...\n");

	if (fractal != NULL && renderData != NULL)
	{
		// Render the fractal
		Fractal_Render(fractal, renderData, renderWidth, renderHeight);

		// Update the OpenGL texture
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, renderWidth, renderHeight,
		             0, GL_RGBA, GL_UNSIGNED_BYTE, renderData);
		if (glGetError() != GL_NO_ERROR)
			printf("WARNING: Update of texture failed (OpenGL).\n");
	}
}

/**
 * Changes the fractal which is being displayed.
 * WARNING: This does NOT rerender the fractal and refresh the window.
 */
static void changeFractal(struct Fractal *f)
{
	printf("Setting fractal\n");

	if (fractal != NULL)
		fractal->destroy(fractal);
	fractal = f;

	// Figure out a good window size. This transformation has the properties:
	// - Window area is conserved
	// - Fractal X and Y axis have same scale
	if (fractal != NULL)
	{
		int windowWidth, windowHeight;
		glfwGetWindowSize(&windowWidth, &windowHeight);
		int windowArea = windowWidth * windowHeight;
		double fractalWidth = fractal->maxX - fractal->minX;
		double fractalHeight = fractal->maxY - fractal->minY;
		double fractalRatio = fractalWidth / fractalHeight;

		int newWidth = (int)sqrt(windowArea * fractalRatio);
		int newHeight = (int)sqrt(windowArea / fractalRatio);
		if (newWidth == 0)
			newWidth = 1;
		if (newHeight == 0)
			newHeight = 1;

		glfwSetWindowSize(newWidth, newHeight);
	}
}

/**
 * Changes the size of the rendered fractal render image.
 */
static void changeRenderSize(int w, int h)
{
	printf("Setting render size: %ix%i\n", w, h);

	renderWidth = w;
	renderHeight = h;

	// Reallocate buffer
	free(renderData);
	renderData = malloc(w*h*4);
	if (renderData == NULL)
		printf("WARNING! Couldn't allocate enough memory.\n");
}

/**
 * Draws the fractal to the current OpenGL buffer.
 * WARNING: Does not flush / swap buffers.
 */
static void refreshFractalGL(void)
{
	printf("Displaying...\n");

	glClear(GL_COLOR_BUFFER_BIT);
	
	int windowWidth, windowHeight;
	glfwGetWindowSize(&windowWidth, &windowHeight);
	glViewport(0, 0, windowWidth, windowHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 1, 1, 0, 0, 1);
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();

	// Create a quad covering the whole screen, with the fractal texture mapped
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(1.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(1.0f, 1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, 1.0f);
	glEnd();

	if (glGetError() != GL_NO_ERROR)
		printf("WARNING: OpenGL rendering failed.\n");
}

/******************
 * EVENT HANDLING *
 ******************/
static GLFWCALL void keyCallback(int key, int action)
{
	if (action == GLFW_PRESS)
	{
		if (key >= '1' && key <= '6')
		{
			struct Fractal *fractal = NULL;

			switch ((char)key)
			{
				case '1': fractal = MandelbrotFractal_Create(); break;
				case '2': fractal = JuliaFractal_X2m075_Create(); break;
				case '3': fractal = JuliaFractal_X2mI_Create(); break;
				case '4': fractal = JuliaFractal_X4me2X_Create(); break;
				case '5': fractal = NewtonFractal_X3m1_Create(); break;
				case '6': fractal = NewtonFractal_X3m2Xp2_Create(); break;
			}

			changeFractal(fractal);
			renderFractal();
			redraw = 1;
		}
		else if (key == GLFW_KEY_F1)
		{
			changeRenderSize(renderWidth*2,renderHeight*2);
			renderFractal();
			redraw = 1;
		}
		else if (key == GLFW_KEY_F2)
		{
			if ((renderWidth % 2) == 0 && (renderHeight % 2) == 0)
			{
				changeRenderSize(renderWidth/2,renderHeight/2);
				renderFractal();
				redraw = 1;
			}
		}
		else if (key == GLFW_KEY_F3)
		{
			int windowWidth, windowHeight;
			glfwGetWindowSize(&windowWidth, &windowHeight);
			if (windowWidth <= 8192 && windowHeight <= 8192)
			{
				glfwSetWindowSize(windowWidth*2, windowHeight*2);
				redraw = 1;
			}
		}
		else if (key == GLFW_KEY_F4)
		{
			int windowWidth, windowHeight;
			glfwGetWindowSize(&windowWidth, &windowHeight);
			if (windowWidth >= 2 && windowHeight >= 2)
			{
				glfwSetWindowSize(windowWidth/2, windowHeight/2);
				redraw = 1;
			}
		}
		else if (key == GLFW_KEY_F5)
		{
			if (!SaveToTGA("fractal.tga", renderData, renderWidth, renderHeight))
				printf("WARNING: Error saving to TGA file.\n");
		}
	}
}

static GLFWCALL void mousePosCallback(int x, int y)
{
	if (dragging)
	{
		double fractalVector[2];
		windowToFractalVector((int[]){lastMousePos[0] - x,lastMousePos[1] - y},
		                      fractalVector);

		Fractal_Translate(fractal, fractalVector[0], fractalVector[1]);
		renderFractal();
		redraw = 1;
	}

	// Update saved pos
	lastMousePos[0] = x;
	lastMousePos[1] = y;
}

static GLFWCALL void mouseCallback(int button, int action)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		dragging = (action == GLFW_PRESS);
	}
	else if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
	{
		double fractalCoords[2];
		windowToFractalCoords(lastMousePos, fractalCoords);
		printf("You have clicked over the point (%f)+(%f)I\n",
			fractalCoords[0], fractalCoords[1]);
	}
}

static GLFWCALL void mouseWheelCallback(int pos)
{
	// Handle zooming
	if (pos - lastMouseWheelPos != 0)
	{
		// pos increments when scrolling up and decrements when scrolling down
		double scale = pow(2.0/3.0, pos - lastMouseWheelPos);

		double fractalCoords[2];
		windowToFractalCoords(lastMousePos, fractalCoords);

		Fractal_Zoom(fractal, scale, fractalCoords[0], fractalCoords[1]);
		renderFractal();
		redraw = 1;
	}
	
	// Update saved pos
	lastMouseWheelPos = pos;
}

static GLFWCALL int windowCloseCallback(void)
{
	running = 0;
	return GL_TRUE;
}

static GLFWCALL void windowRefreshCallback(void)
{
	redraw = 1;
}

int main(int argc, char **argv)
{
	// Print usage info
	printf(
		"Fractal Viewer - by Joan Bruguera\n"
		"\n"
		"Fractal Selection:\n"
		"- 1: Mandelbrot Set\n"
		"- 2: Julia Set - Polynomial x^2-0.75\n"
		"- 3: Julia Set - Polynomial x^2-I\n"
		"- 4: Julia Set - Function x^4-e^(2x)\n"
		"- 5: Newton's Fractal - Polynomial x^3-1\n"
		"- 6: Newton's Fractal - Polynomial x^3-2x+2\n"
		"\n"
		"Commands:\n"
		"- F1: Increase rendering quality\n"
		"- F2: Decrease rendering quality\n"
		"- F3: Increase window size\n"
		"- F4: Decrease window size\n"
		"- F5: Save rendered image as TGA (current directory, fractal.tga)\n"
		"\n"
		"Mouse usage:\n"
		"- Click and drag to move the fractal\n"
		"- Use the mouse wheel to zoom\n"
		"- Click with the middle button to get information about a point\n"
		"\n"
	);

	// Create our rendering window
	if (!glfwInit())
	{
		printf("ERROR: Can't initialize GLFW.\n");
		return -1;
	}
	
	if (!glfwOpenWindow(512, 512, 8, 8, 8, 8, 0, 0, GLFW_WINDOW))
	{
		printf("ERROR: Can't create the GLFW window.\n");
		return -1;
	}
	
	glfwSetWindowTitle("Fractal Viewer");
	glfwSetKeyCallback(keyCallback);
	glfwSetMouseButtonCallback(mouseCallback);
	glfwSetMousePosCallback(mousePosCallback);
	glfwSetMouseWheelCallback(mouseWheelCallback);
	glfwSetWindowCloseCallback(windowCloseCallback);
	glfwSetWindowRefreshCallback(windowRefreshCallback);

	// Enable texturing and disable depth (we're only doing 2D)
	glEnable(GL_TEXTURE_2D);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
 	glEnable(GL_BLEND);

	glDisable(GL_DEPTH_TEST);

	// Create the texture where we're going to render the fractal
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	if (glGetError() != GL_NO_ERROR)
	{
		printf("ERROR: Can't set up the OpenGL context.\n");
		return -1;
	}

	// Set up the rendering parameters
	changeFractal(MandelbrotFractal_Create());
	changeRenderSize(1024, 1024);
	renderFractal();
	
	// Main loop
	glfwGetMousePos(&lastMousePos[0], &lastMousePos[1]);
	lastMouseWheelPos = glfwGetMouseWheel();

	while (running)
	{
		if (redraw)
		{
			refreshFractalGL();
			glfwSwapBuffers();
			redraw = 0;
		}
		else
		{
			glfwWaitEvents();
		}
	}
	
	// Cleanup
	free(renderData);
	glDeleteTextures(1, &texture);
	glfwCloseWindow();	
	glfwTerminate();

	return 0;
}
