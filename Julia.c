/* Julia.c: Code for Julia Set
 * Author: Joan Bruguera
 * License: WTFPL */
#include "Julia.h"
#include <stdlib.h>
#include <complex.h>

/***********************************
 * PRIVATE STRUCTURE FOR JULIA SET *
 ***********************************/
struct JuliaFractal
{
	/** Base class members */
	struct Fractal base;
	/** Iteration function */
	complex double (*iterationFunc)(complex double x);
	/** Radius at which we consider a point has escaped */
	double escapeRadius;
};

/**********************************
 * RENDERING CODE FOR JULIA'S SET *
 **********************************/
static int Julia_Iterate(const struct JuliaFractal *jfractal, complex double c)
{
	// Make a measure of how quickly it escapes in range [0x00,0xFF]
	complex double z = c;
	for (int i = 0; i < 64; i++)
	{
		z = jfractal->iterationFunc(z);
		if (cabs(z) >= jfractal->escapeRadius)
			return i*4;
	}

	return 0xFF; // Likely belongs to the set
}

void JuliaFractal_Render(const struct Fractal *fractal, uint8_t *pixels,
                         int width, int height,
                         double minX, double maxX, double minY, double maxY)
{
	const struct JuliaFractal *jfractal = (const struct JuliaFractal *)fractal;

	uint32_t *pixels32 = (uint32_t *)pixels;
	double sx = (maxX - minX) / width;
	double sy = (maxY - minY) / height;

	double cy = maxY;
	for (int y = height-1; y >= 0; y--, cy -= sy)
	{
		double cx = minX;
		for (int x = 0; x < width; x++, cx += sx)
		{
			int it = Julia_Iterate(jfractal, cx+cy*I);
			*pixels32++ = makeColor32(0xFF-it, 0xFF-it, 0xFF-it, 0xFF);
		}
	}
}

/***********************************************
 * CONSTRUCTOR/DESTRUCTOR CODE FOR JULIA'S SET *
 ***********************************************/
void JuliaFractal_Destroy(struct Fractal *fractal)
{
	free(fractal);
}

struct Fractal *JuliaFractal_Create(
	double minX, double maxX,
	double minY, double maxY,
	complex double (*iterationFunc)(complex double x),
	double escapeRadius
)
{
	struct JuliaFractal *jfractal = malloc(sizeof(struct JuliaFractal));
	if (jfractal == NULL)
		return NULL;

	jfractal->base.renderArea = JuliaFractal_Render;
	jfractal->base.destroy = JuliaFractal_Destroy;
	jfractal->base.minX = minX;
	jfractal->base.maxX = maxX;
	jfractal->base.minY = minY;
	jfractal->base.maxY = maxY;
	jfractal->iterationFunc = iterationFunc;
	jfractal->escapeRadius = escapeRadius;
	return (struct Fractal *)jfractal;
}

/********************************
 * JULIA'S SET SAMPLE: X^2-0.75 *
 ********************************/
static complex double X2m075(complex double x)
{
	return x*x-0.75;
}

struct Fractal *JuliaFractal_X2m075_Create(void)
{
	return JuliaFractal_Create(-2, 2, -2, 2, X2m075, 4);
}

/*****************************
 * JULIA'S SET SAMPLE: X^2-I *
 *****************************/
static complex double X2mI(complex double x)
{
	return x*x-I;
}

struct Fractal *JuliaFractal_X2mI_Create(void)
{
	return JuliaFractal_Create(-2, 2, -2, 2, X2mI, 4);
}

/**********************************
 * JULIA'S SET SAMPLE: X^4-e^(2X) *
 **********************************/
static complex double X4me2X(complex double x)
{
	return x*x*x*x-cexp(2*x);
}

struct Fractal *JuliaFractal_X4me2X_Create(void)
{
	return JuliaFractal_Create(-2, 2, -2, 2, X4me2X, 4);
}
